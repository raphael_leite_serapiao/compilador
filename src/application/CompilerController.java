// ***************** INTERFACE ***************** //
//TODO: o sistema está aceitando salvar arquivos com nome em branco
//TODO: abrir arquivo (quebrado)
//TODO: ícones não aparecendo
//TODO: possibilitar salvar qualquer extensão de arquivo
//TODO: erro linhas e colunas
//TODO: linkar as variáveis de contagem de erros com a tela

// *****************   JAVACC  ***************** //
//TODO: bloco de comentários não está contando
//TODO: verificar com o alessandro o que é preciso nas mensagens de erros


package application;

import application.javacc._ErrorManager;
import application.javacc.langX;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class CompilerController implements Initializable {

    Clipboard systemClipboard = Clipboard.getSystemClipboard();

    private File currentFile;

    @FXML
    private Label lblQtLines;

    @FXML
    private Label lblQtColumns;

    @FXML
    private Label messageLabel;

    @FXML
    private TextArea txtEditCode;

    @FXML
    private TextArea txtMessages;

    @FXML
    private TitledPane headerTitle;

    @FXML
    private Button imgButtonNewFile;

    @FXML
    private Button imgButtonOpenFile;

    @FXML
    private Button imgButtonSaveFile;

    @FXML
    private Button imgButtonCompile;

    @FXML
    private Button imgButtonExecute;

    @FXML
    private Button imgButtonCopy;

    @FXML
    private Button imgButtonCut;

    @FXML
    private Button imgButtonPaste;

    @FXML
    private Button imgButtonChangeColor;

    private langX l;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeImgButtonActions(imgButtonNewFile, "file.png", "Novo Arquivo (⌃⇧N)");
        initializeImgButtonActions(imgButtonOpenFile, "folder.png", "Abrir Arquivo (⌃⇧O)");
        initializeImgButtonActions(imgButtonSaveFile, "diskette.png", "Salvar Arquivo (⌃⇧S)");
        initializeImgButtonActions(imgButtonCompile, "wrench.png", "Compilar (⌃⇧B)");
        initializeImgButtonActions(imgButtonExecute, "play.png", "Executar (⌃⇧R)");
        initializeImgButtonActions(imgButtonCopy, "copy.png", "Copiar (⌃⇧C)");
        initializeImgButtonActions(imgButtonCut, "cut.png", "Cortar (⌃⇧X)");
        initializeImgButtonActions(imgButtonPaste, "paste.png", "Colar (⌃⇧V)");
        initializeImgButtonActions(imgButtonChangeColor, "palette.png", "Alterar Tema");

        txtMessages.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                txtMessages.setScrollTop(Double.MAX_VALUE);
            }
        });

        openFile2();
    }

    private void initializeImgButtonActions(Button button, String imageName, String tooltip) {
        File file = new File(".//src//application//images//" + imageName);
        Image image = new Image(file.toURI().toString());
        ImageView imgView = new ImageView(image);
        imgView.setFitHeight(15);
        imgView.setFitWidth(15);
        button.setGraphic(imgView);
        button.setTooltip(new Tooltip(tooltip));
    }

    @FXML
    private void copy(ActionEvent event) {
        String text = getSelectedText();
        ClipboardContent content = new ClipboardContent();
        content.putString(text);
        systemClipboard.setContent(content);
    }

    @FXML
    private void cut(ActionEvent event) {
        String text = txtEditCode.getSelectedText();
        ClipboardContent content = new ClipboardContent();
        content.putString(text);
        systemClipboard.setContent(content);
        IndexRange range = txtEditCode.getSelection();
        String origText = txtEditCode.getText();
        String firstPart = origText.substring(0, range.getStart());
        String lastPart = origText.substring(range.getEnd(), origText.length());
        txtEditCode.setText(firstPart + lastPart);
        txtEditCode.positionCaret(range.getStart());
    }

    @FXML
    private void paste(ActionEvent event) {
        String clipboardText = systemClipboard.getString();
        IndexRange range = txtEditCode.getSelection();
        String origText = txtEditCode.getText();
        int endPos = 0;
        String updatedText = "";
        String firstPart = origText.substring(0, range.getStart());
        String lastPart = origText.substring(range.getEnd(), origText.length());
        updatedText = firstPart + clipboardText + lastPart;
        if (range.getStart() == range.getEnd()) {
            endPos = range.getEnd() + clipboardText.length();
        } else {
            endPos = range.getStart() + clipboardText.length();
        }
        txtEditCode.setText(updatedText);
        txtEditCode.positionCaret(endPos);
    }

    @FXML
    private void saveAs(ActionEvent event) {
        inputSaveFile();
    }

    @FXML
    private void save(ActionEvent event) {
        if (currentFile != null) {
            saveFile(currentFile.getPath(), true);
        } else {
            saveAs(event);
        }
    }

    @FXML
    private void newFile(ActionEvent event) {
        if (!verifyMustSaveFileAndNextStep()) {
            return;
        }
        cleanTextFields();
    }

    @FXML
    private void exit(ActionEvent event) {
        if (!verifyMustSaveFileAndNextStep()) {
            return;
        }
        Platform.exit();
    }

    @FXML
    private void openFile(ActionEvent event) {
        if (!verifyMustSaveFileAndNextStep()) {
            return;
        }
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Abrir um arquivo");
        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        fileChooser.setInitialDirectory(new File(currentPath));
        File file = fileChooser.showOpenDialog(new Stage());
        if (file != null) {
            txtEditCode.setText(readFile(file));
            currentFile = file;
            headerTitle.setText("Compilador [" + file.getName() + "]");
        }
    }


    private void openFile2() {
        File file = new File("/Users/raphael/eclipse-workspace/Compilador/ccc2");
        txtEditCode.setText(readFile(file));
        currentFile = file;
        headerTitle.setText("Compilador [" + file.getName() + "]");
    }


    @FXML
    private void mouseOnTxtEditCodeReleased(MouseEvent event) {
        cursorTxtEditCodeChanged();
    }

    @FXML
    private void keyOnTxtEditCodeReleased(KeyEvent event) {
        cursorTxtEditCodeChanged();
    }

    @FXML
    private void compile(ActionEvent event) {

        txtMessages.setText("");
        messageLabel.setText("");
        _ErrorManager.getErroslexicos().clear();
        _ErrorManager.getErrosSintaticos().clear();
        String result = "";

        if (!isFileOk())
            return;

        try {
            l._start(currentFile.getName());

            if (!_ErrorManager.getErroslexicos().isEmpty()) {
                messageLabel.setText("Erros Léxicos Encontrados: " + _ErrorManager.getErroslexicos().size());
                for (String str : _ErrorManager.getErroslexicos())
                    result += str;
            } else if (!_ErrorManager.getErrosSintaticos().isEmpty()) {
                messageLabel.setText("Erros Sintáticos Encontrados: " + _ErrorManager.getErrosSintaticos().size());
                for (String str : _ErrorManager.getErrosSintaticos())
                    result += str;
            } else {
                messageLabel.setText("Compilado com sucesso");
            }
        } catch (Exception e) {
            errorDialog(e);
        }

        txtMessages.setText(result);
    }

    private Boolean isFileOk() {
        Boolean fileOk = true;
        if (txtEditCode.getText().isEmpty()) {
            resultDialog("Não há código para ser Compilado!", AlertType.ERROR);
            fileOk = false;
        } else if (currentFile == null && !mustSaveBeforeCompileDialog()) {
            fileOk = false;
        } else if (currentFile == null && !inputSaveFile()) {
            fileOk = false;
        } else {
            saveFile(currentFile.getPath(), false);
        }
        return fileOk;
    }

    @FXML
    private void execute(ActionEvent event) {

        if(!_ErrorManager.getErroslexicos().isEmpty() || !_ErrorManager.getErrosSintaticos().isEmpty()) {
            resultDialog("Existem erros sintáticos ou léxicos no código a ser Compilado!", AlertType.ERROR);
            return;
        }

        String result = "";

        try {
            l._runSemantic(currentFile.getName());
            if(!l.semantic.erros.isEmpty()) {
                messageLabel.setText("Erros Semânticos Encontrados: " + l.semantic.erros.size());
                for (String str : l.semantic.erros) {
                    result += str + "\n";
                }
            } else {
                TableObject tblo = new TableObject();
                tblo.setInstructions(l.semantic.getAreaDeInstrucoes());
                tblo.updateTable();
                tblo.setVisible(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.print("\033[H\033[2J");
        System.out.flush();
        System.out.println(l.semantic.toString());
        txtMessages.setText(result);
    }


    // ------------------------- MÉTODOS PÚBLICOS ------------------------- //
    public Boolean verifyMustSaveFileAndNextStep() {
        if (!txtEditCode.getText().isEmpty()) {
            if (currentFile != null) {
                if (verifyFileWasModified() && !saveFileOptionsDialog()) {
                    return false;
                }
            } else if (!saveFileOptionsDialog()) {
                return false;
            }
        }

        return true;
    }


    // ------------------------- MÉTODOS PRIVATES ------------------------- //
    private Boolean inputSaveFile() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Escolha um lugar para Salvar o arquivo.");
        String currentPath = Paths.get(".").toAbsolutePath().normalize().toString();
        chooser.setInitialDirectory(new File(currentPath));

        File selectedFile = null;

        selectedFile = chooser.showSaveDialog(null);

        //&& !mustOverwriteFileDialog()
        if (selectedFile == null) {
            return false;
        }

        return saveFile(selectedFile.getPath(), true);
    }

    private Boolean verifyFileWasModified() {
        String currentCode = txtEditCode.getText().replaceAll("\\n+$", "");
        String fileCode = readFile(currentFile).replaceAll("\\n+$", "");

        System.out.print(currentCode);
        System.out.print(fileCode);

        if (!currentCode.equals(fileCode)) {
            return true;
        }

        return false;
    }

    private String getSelectedText() {
        TextArea[] tfs = new TextArea[]{txtEditCode, txtMessages};
        for (TextArea tf : tfs) {
            if (!tf.getSelectedText().isEmpty()) {
                return tf.getSelectedText();
            }
        }

        return "";
    }

    private void cursorTxtEditCodeChanged() {
        String[] lines = txtEditCode.textProperty().get().substring(0, txtEditCode.getCaretPosition()).split("\n", -1);
        for (int i = 0; i < lines.length; i++) {
            lines[i] = lines[i].replaceAll("\t", "    ");
        }


        int currentLine = lines.length;
        int currentColumn = lines[lines.length - 1].length() + 1;
        lblQtLines.setText(Integer.toString(currentLine));
        lblQtColumns.setText(Integer.toString(currentColumn));
    }

    private void cleanTextFields() {
        currentFile = null;
        headerTitle.setText("Compilador");
        txtEditCode.setText("");
        txtMessages.setText("");
    }

    private Boolean saveFile(String fileDirectory, Boolean mustShowMessage) {
        Path pathFile = Paths.get(fileDirectory);
        String[] textSplited = txtEditCode.getText().split("\n");
        List<String> list = Arrays.asList(textSplited);
        try {
            Files.write(pathFile, list, Charset.forName("UTF-8"));
            currentFile = pathFile.toFile();
            headerTitle.setText("Compilador [" + currentFile.getName() + "]");
            if (mustShowMessage) {
                fileSavedOK();
            }
            return true;
        } catch (Exception ex) {
            errorDialog(ex);
        }
        return false;
    }

    private String readFile(File file) {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            StringBuilder stringBuffer = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuffer.append(line + "\n");
            }
            return stringBuffer.toString();
        } catch (IOException ex) {
            errorDialog(ex);
        }
        return "";
    }

    private void fileSavedOK() {
        resultDialog("Arquivo salvo com sucesso!", AlertType.INFORMATION);
    }


    // ------------------------- DIALOGS ------------------------- //
    private String inputDialog() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Compilador");
        dialog.setHeaderText("Qual será o nome do arquivo?");
        dialog.setContentText(null);
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            return result.get();
        }
        return null;
    }

    private void resultDialog(String text, AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle("Compilador");
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }

    private void errorDialog(Exception ex) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Compilador");
        alert.setHeaderText("Ops! Aconteceu algum problema no percurso!");
        alert.setContentText(ex.getMessage());

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);
        alert.getDialogPane().setExpandableContent(expContent);
        alert.showAndWait();
    }

    private Boolean mustOverwriteFileDialog() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Compilador");
        alert.setHeaderText("Arquivo com esse nome já existe!");
        alert.setContentText("Você deseja sobrescrever o arquivo?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            return true;
        }

        return false;
    }

    private Boolean mustSaveBeforeCompileDialog() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Compilador");
        alert.setHeaderText("Ops, o arquivo ainda não foi salvo!");
        alert.setContentText("Você deseja salvar o arquivo para continuar a Compilar?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            return true;
        }

        return false;
    }

    private Boolean saveFileOptionsDialog() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Compilador");
        alert.setHeaderText("Atenção!");
        alert.setContentText("Você deseja salvar sua sessão atual?");

        ButtonType buttonTypeOne = new ButtonType("Sim");
        ButtonType buttonTypeTwo = new ButtonType("Não");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeCancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            if (currentFile != null) {
                save(null);
            } else {
                saveAs(null);
            }
            cleanTextFields();
        } else if (result.get() == buttonTypeTwo) {
            cleanTextFields();
        } else {
            return false;
        }
        return true;
    }
}
