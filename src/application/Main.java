package application;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class Main extends Application {

    public static CompilerController compilerController;
    private String mainStyle = getClass().getResource("application.css").toExternalForm();
    private String darkStyle = getClass().getResource("applicationDark.css").toExternalForm();

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("Compiler.fxml"));
            BorderPane rootLayout = loader.load();
            TitledPane root = (TitledPane) rootLayout.getChildren().get(1);
            ToolBar toolbar = (ToolBar) root.getContent();
            ObservableList<Node> nodes = toolbar.getItems();
            Button imgButtonChangeColor = (Button) nodes.get(15);

            Scene scene = new Scene(rootLayout, 900, 600);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

            imgButtonChangeColor.setOnAction(ev -> {
                if (scene.getStylesheets().contains(mainStyle)) {
                    scene.getStylesheets().clear();
                    scene.getStylesheets().add(darkStyle);
                } else {
                    scene.getStylesheets().clear();
                    scene.getStylesheets().add(mainStyle);
                }
            });

            compilerController = loader.getController();

            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    if (!compilerController.verifyMustSaveFileAndNextStep()) {
                        event.consume();
                    }
                }
            });

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        System.out.flush();
        launch(args);
    }
}