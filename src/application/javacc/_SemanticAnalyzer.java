/**
 * @author Raphael Leite Serapiao
 */

package application.javacc;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class union {
    public Integer categoria;
    public Integer atr1;
    public Integer atr2;

    public union(Integer categoria, Integer atr1, Integer atr2) {
        this.categoria = categoria;
        this.atr1 = atr1;
        this.atr2 = atr2;
    }

    @Override
    public String toString() {
        return " {" +
                "categoria=" + categoria +
                ", atr1=" + atr1 +
                ", atr2='" + atr2 + '\'' +
                '}';
    }
}

public class _SemanticAnalyzer {

    public enum CONTEXTO {ENTRADA_DADOS, ATRIBUICAO, AS_CONSTANT, AS_VARIABLE, NENHUM}

    public enum SAIDA {WRITE_ALL_THIS, WRITE_THIS, NENHUM}

    private final String ERRO1 = "identificador já foi declarado anteriormente -> ";
    private final String ERRO2 = "identificador de variável não indexada -> ";
    private final String ERRO3 = "identificador de variável indexada exige um índice -> ";
    private final String ERRO4 = "identificador não foi declarado ou o identificador é " +
            "usado como identificador inicial do programa ou então ele é uma constante/tipo enumerado -> ";
    private final String ERRO5 = "tipo inválido para constante de tipo logic";
    private final String ERRO6 = "tipo inválido para constante de tipo enumerado";
    private final String ERRO7 = "o identificador não foi declarado ou o identificador é " +
            "usado como identificador inicial do programa ou então ele é um tipo enumerado -> ";
    private final String ERRO8 = "identificador de constante ou de variável não indexada -> ";

    private CONTEXTO contexto = CONTEXTO.NENHUM;
    private Integer vt = 0;
    private Integer vp = 0;
    private Integer vi = 0;
    private Integer tvi = 0;
    private Integer ponteiro = 1;
    private Boolean variavelIndexada = null;
    private Stack<Integer> pilhaDeDesvios = new Stack<>();
    private List<Instruction> areaDeInstrucoes = new ArrayList<>();
    private Map<String, union> tabelaDeSimbolos = new LinkedHashMap<>();
    private Map<String, union> tupla = new HashMap();
    private Integer tipo = null;
    private SAIDA saida = SAIDA.NENHUM;

    public List<String> erros = new ArrayList<>();
    private Map<String, List<String>> tabelaDeTiposEnumerados = new LinkedHashMap<>();
    private List<Integer> listaDeAtributos = new ArrayList<>();
    private Integer constIntArmazenadaAcao12 = null;
    private String idArmazenadoAcao10 = "";
    private String idArmazenadoAcao24 = "";
    public Boolean execute = false;


    /*
     * ação #1:
     * reconhecimento do identificador de programa
     */
    public void action1(String token) {
        if (!execute) return;
        tabelaDeSimbolos.put(token, new union(0, null, null));
    }


    /*
     * ação #2:
     * reconhecimento de fim de programa
     */
    public void action2() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "STP", new Item(0)));
    }


    /*
     * ação #3:
     * reconhecimento de identificador de tipo enumerado
     */
    public void action3(String token) {
        if (!execute) return;
        if (tabelaDeSimbolos.containsKey(token) || tabelaDeTiposEnumerados.containsKey(token)) {
            erros.add(ERRO1 + token);
            return;
        }
        tabelaDeTiposEnumerados.put(token, new ArrayList<>());
    }


    /*
     * ação #4:
     * reconhecimento de identificador de constante de tipo enumerado
     */
    public void action4(String token) {
        if (!execute) return;
        if (identificadorJaDeclarado(token)) {
            erros.add(ERRO1 + token);
            return;
        }

        List<String> list = null;
        Iterator<Map.Entry<String, List<String>>> it = tabelaDeTiposEnumerados.entrySet().iterator();

        while (it.hasNext())
            list = it.next().getValue();

        if (list != null)
            list.add(token);
    }


    /*
     * ação #5:
     * reconhecimento das palavras reservadas as constant
     */
    public void action5() {
        if (!execute) return;
        contexto = CONTEXTO.AS_CONSTANT;
        vi = 0;
        tvi = 0;
    }


    /*
     * ação #6:
     * reconhecimento do término da declaração de constantes ou variáveis de um determinado tipo
     */
    public void action6() {
        if (!execute) return;
        List<String> keyList = new ArrayList<>(tabelaDeSimbolos.keySet());
        int last_index = keyList.size() - 1;
        for (int i = last_index; i > last_index - (vp + vi); --i)
            tabelaDeSimbolos.get(keyList.get(i)).categoria = tipo;

        vp += tvi;

        switch (tipo) {
            case 1:
            case 5:
                areaDeInstrucoes.add(new Instruction(ponteiro, "ALI", new Item(vp)));
                ponteiro++;
                break;
            case 2:
            case 6:
                areaDeInstrucoes.add(new Instruction(ponteiro, "ALR", new Item(vp)));
                ponteiro++;
                break;
            case 3:
            case 7:
                areaDeInstrucoes.add(new Instruction(ponteiro, "ALS", new Item(vp)));
                ponteiro++;
                break;
            case 4:
                areaDeInstrucoes.add(new Instruction(ponteiro, "ALB", new Item(vp)));
                ponteiro++;
                break;
        }

        if (tipo == 1 || tipo == 2 || tipo == 3 || tipo == 4) {
            vp = 0;
            vi = 0;
            tvi = 0;
        }
    }


    /*
     * ação #7:
     * reconhecimento de valor na declaração de constante
     */
    public void action7(Token token) {
        if (!execute) return;
        switch (tipo) {
            case 5:
                areaDeInstrucoes.add(new Instruction(ponteiro, "LDI", new Item(Integer.parseInt(token.image))));
                ponteiro++;
                break;
            case 6:
                areaDeInstrucoes.add(new Instruction(ponteiro, "LDR", new Item(Float.parseFloat(token.image))));
                ponteiro++;
                break;
            case 7:
                areaDeInstrucoes.add(new Instruction(ponteiro, "LDS", new Item(token.image)));
                ponteiro++;
                break;
        }
        areaDeInstrucoes.add(new Instruction(ponteiro, "STC", new Item(vp)));
        ponteiro++;
        vp = 0;
    }


    /*
     * ação #8:
     * reconhecimento das palavras reservadas as variable
     */
    public void action8() {
        if (!execute) return;
        contexto = CONTEXTO.AS_VARIABLE;
    }


    /*
     * ação #9:
     * reconhecimento de identificador de constante
     */
    public void action9(String token) {
        if (!execute) return;
        if (identificadorJaDeclarado(token)) {
            erros.add(ERRO1 + token);
            return;
        }
        vt++;
        vp++;
        tabelaDeSimbolos.put(token, new union(null, vt, null));
    }


    /*
     * ação #10:
     * reconhecimento de identificador de variável
     */
    public void action10(String token) {
        if (!execute) return;
        if (contexto == CONTEXTO.AS_VARIABLE) {
            if (identificadorJaDeclarado(token)) {
                erros.add(ERRO1 + token);
                return;
            }
            variavelIndexada = false;
            idArmazenadoAcao10 = token;
        } else if (contexto == CONTEXTO.ATRIBUICAO || contexto == CONTEXTO.ENTRADA_DADOS) {
            variavelIndexada = false;
            idArmazenadoAcao10 = token;
        }
    }


    /*
     * ação #11:
     * reconhecimento de identificador de variável e tamanho da variável indexada
     */
    public void action11() {
        if (!execute) return;

        if (contexto == CONTEXTO.AS_VARIABLE) {

            if (!variavelIndexada) {
                vt++;
                vp++;
                tabelaDeSimbolos.put(idArmazenadoAcao10, new union(null, vt, null));
            } else {
                vi++;
                tvi += constIntArmazenadaAcao12;
                tabelaDeSimbolos.put(idArmazenadoAcao10,
                        new union(null, vt + 1, constIntArmazenadaAcao12));
                vt += constIntArmazenadaAcao12;
            }

        } else if (contexto == CONTEXTO.ATRIBUICAO) {

            if (tabelaDeSimbolos.containsKey(idArmazenadoAcao10) && isIdentificadorDeVariavel(idArmazenadoAcao10)) {
                Integer atr1 = tabelaDeSimbolos.get(idArmazenadoAcao10).atr1;
                Integer atr2 = tabelaDeSimbolos.get(idArmazenadoAcao10).atr2;
                if (atr2 == null) {
                    if (!variavelIndexada) {
                        listaDeAtributos.add(atr1);
                    } else {
                        erros.add(ERRO2 + idArmazenadoAcao10);
                        return;
                    }
                } else {
                    if (variavelIndexada) {
                        listaDeAtributos.add(atr1 + constIntArmazenadaAcao12 - 1);
                    } else {
                        erros.add(ERRO3 + idArmazenadoAcao10);
                        return;
                    }
                }
            } else {
                erros.add(ERRO4 + idArmazenadoAcao10);
                return;
            }

        } else if (contexto == CONTEXTO.ENTRADA_DADOS) {

            if (tabelaDeSimbolos.containsKey(idArmazenadoAcao10) && isIdentificadorDeVariavel(idArmazenadoAcao10)) {
                Integer atr1 = tabelaDeSimbolos.get(idArmazenadoAcao10).atr1;
                Integer atr2 = tabelaDeSimbolos.get(idArmazenadoAcao10).atr2;
                Integer cat = tabelaDeSimbolos.get(idArmazenadoAcao10).categoria;
                if (atr2 == null) {
                    if (!variavelIndexada) {
                        areaDeInstrucoes.add(new Instruction(ponteiro, "REA", new Item(cat)));
                        ponteiro++;
                        areaDeInstrucoes.add(new Instruction(ponteiro, "STR", new Item(atr1)));
                        ponteiro++;
                    } else {
                        erros.add(ERRO2 + idArmazenadoAcao10);
                        return;
                    }
                } else {
                    if (variavelIndexada) {
                        areaDeInstrucoes.add(new Instruction(ponteiro, "REA", new Item(cat)));
                        ponteiro++;
                        areaDeInstrucoes.add(new Instruction(ponteiro, "STR",
                                new Item(atr1 + constIntArmazenadaAcao12 - 1)));
                        ponteiro++;
                    } else {
                        erros.add(ERRO3 + idArmazenadoAcao10);
                        return;
                    }
                }
            } else {
                erros.add(ERRO4 + idArmazenadoAcao10);
                return;
            }
        }
    }


    /*
     * ação #12:
     * reconhecimento de constante inteira como tamanho da variável indexada
     */
    public void action12(String token) {
        if (!execute) return;
        constIntArmazenadaAcao12 = Integer.valueOf(token);
        variavelIndexada = true;
    }


    /*
     * ação #13:
     * reconhecimento da palavra reservada integer
     */
    public void action13() {
        if (!execute) return;
        tipo = (contexto == CONTEXTO.AS_VARIABLE) ? 1 : 5;
    }


    /*
     * ação #14:
     * reconhecimento da palavra reservada float
     */
    public void action14() {
        if (!execute) return;
        tipo = (contexto == CONTEXTO.AS_VARIABLE) ? 2 : 6;
    }


    /*
     * ação #15:
     * reconhecimento da palavra reservada string
     */
    public void action15() {
        if (!execute) return;
        tipo = (contexto == CONTEXTO.AS_VARIABLE) ? 3 : 7;
    }


    /*
     * ação #16:
     * reconhecimento da palavra reservada boolean
     */
    public void action16() {
        if (!execute) return;
        if (contexto == CONTEXTO.AS_VARIABLE)
            tipo = 4;
        else
            erros.add(ERRO5);
    }


    /*
     * ação #17:
     * reconhecimento de identificador de tipo enumerado
     */
    public void action17() {
        if (!execute) return;
        if (contexto == CONTEXTO.AS_VARIABLE)
            tipo = 1;
        else
            erros.add(ERRO6);
    }


    /*
     * ação #18:
     * reconhecimento do início do comando de atribuição
     */
    public void action18() {
        if (!execute) return;
        contexto = contexto.ATRIBUICAO;
    }


    /*
     * ação #19:
     * reconhecimento do fim do comando de atribuição
     */
    public void action19() {
        if (!execute) return;
        listaDeAtributos.stream().forEach(i -> {
            areaDeInstrucoes.add(new Instruction(ponteiro, "STR", new Item(i)));
            ponteiro++;
        });
        listaDeAtributos.clear();
    }


    //ação #20: reconhecimento do comando de entrada de dados
    public void action20() {
        if (!execute) return;
        contexto = CONTEXTO.ENTRADA_DADOS;
    }


    //ação #21: reconhecimento das palavras reservadas “write all this”
    public void action21() {
        if (!execute) return;
        saida = SAIDA.WRITE_ALL_THIS;
    }


    //ação #22: reconhecimento das palavras reservadas “write this”
    public void action22() {
        if (!execute) return;
        saida = SAIDA.WRITE_THIS;
    }


    //ação #23: reconhecimento de mensagem em comando de saída de dados
    public void action23() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "WRT", new Item(0)));
        ponteiro++;
    }


    //ação #24: reconhecimento de identificador em comando de saída ou em expressão
    public void action24(String token) {
        if (!execute) return;
        if (tabelaDeSimbolos.containsKey(token) && isIdentificadorDeConstanteOuVariavel(token)) {
            variavelIndexada = false;
            idArmazenadoAcao24 = token;
        } else {
            erros.add(ERRO7 + token);
        }
    }


    //ação #25: reconhecimento de identificador de constante ou de
    // variável e tamanho de variável indexada em comando de saída
    public void action25() {
        if (!execute) return;
        Integer atr1 = tabelaDeSimbolos.get(idArmazenadoAcao24).atr1;
        Integer atr2 = tabelaDeSimbolos.get(idArmazenadoAcao24).atr2;

        if (!variavelIndexada) {
            if (atr2 == null) {
                if (saida == SAIDA.WRITE_ALL_THIS) {
                    areaDeInstrucoes.add(
                            new Instruction(ponteiro, "LDS", new Item(idArmazenadoAcao24 + " = ")));
                    ponteiro++;
                    areaDeInstrucoes.add(new Instruction(ponteiro, "WRT", new Item(0)));
                    ponteiro++;
                }
                areaDeInstrucoes.add(new Instruction(ponteiro, "LDV", new Item(atr1)));
                ponteiro++;
            } else {
                erros.add(ERRO3 + idArmazenadoAcao24);
            }

        } else {
            if (atr2 != null) {
                if (saida == SAIDA.WRITE_ALL_THIS) {
                    areaDeInstrucoes.add(new Instruction(ponteiro, "LDS",
                            new Item(idArmazenadoAcao24 + "[" + constIntArmazenadaAcao12 + "] = ")));
                    ponteiro++;
                    areaDeInstrucoes.add(new Instruction(ponteiro, "WRT", new Item(0)));
                    ponteiro++;
                }
                areaDeInstrucoes.add(
                        new Instruction(ponteiro, "LDV", new Item(atr1 + constIntArmazenadaAcao12 - 1)));
                ponteiro++;
            } else {
                erros.add(ERRO8 + idArmazenadoAcao24);
            }
        }
    }

    //ação #26: reconhecimento de constante inteira em comando de saída ou em expressão
    public void action26(String token) {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "LDI", new Item(Integer.parseInt(token))));
        ponteiro++;
    }

    //ação #27: reconhecimento de constante real em comando de saída ou em expressão
    public void action27(String token) {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "LDR", new Item(Float.parseFloat(token))));
        ponteiro++;
    }

    //ação #28: reconhecimento de constante literal em comando de saída ou em expressão
    public void action28(String token) {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "LDS", new Item(token)));
        ponteiro++;
    }

    //ação #29: reconhecimento de fim de comando de seleção
    public void action29() {
        if (!execute) return;
        Integer endereçoInstruçãoDesvioAcao30_31_32 = pilhaDeDesvios.pop();
        areaDeInstrucoes
                .stream()
                .filter(i -> i.getPointer() == endereçoInstruçãoDesvioAcao30_31_32)
                .findFirst()
                .ifPresent(i -> i.setParameter(new Item(ponteiro)));
    }

    //ação #30: reconhecimento da palavra reservada true
    public void action30() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "JMF", new Item("?")));
        ponteiro++;
        pilhaDeDesvios.push(ponteiro - 1);
    }

    //ação #31: reconhecimento da palavra reservada untrue
    public void action31() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "JMT", new Item("?")));
        ponteiro++;
        pilhaDeDesvios.push(ponteiro - 1);
    }

    //ação #32: reconhecimento da palavra reservada untrue (ou true)
    public void action32() {
        if (!execute) return;
        //desempilhar da pilha de desvios o endereço da instrução de desvio empilhado na ação #30 ou #31
        Integer endereçoInstruçãoDesvioAcao30_31 = pilhaDeDesvios.pop();

        //atualizar a instrução de desvio com: endereço = ponteiro + 1
        areaDeInstrucoes
                .stream()
                .filter(i -> i.getPointer() == endereçoInstruçãoDesvioAcao30_31)
                .findFirst()
                .ifPresent(i -> i.setParameter(new Item(ponteiro + 1)));

        //gerar instrução:(ponteiro, JMP, ?),onde endereço = ?
        areaDeInstrucoes.add(new Instruction(ponteiro, "JMP", new Item("?")));
        ponteiro++;

        //empilhar(ponteiro - 1) em pilha de desvios, ou seja, o endereço da instrução JMP
        pilhaDeDesvios.push(ponteiro - 1);

    }

    //ação #33: reconhecimento do início de expressão em comando de repetição
    public void action33() {
        if (!execute) return;
        pilhaDeDesvios.push(ponteiro);
    }

    //ação #34: reconhecimento de expressão em comando de repetição
    public void action34() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "JMF", new Item("?")));
        ponteiro++;
        pilhaDeDesvios.push(ponteiro - 1);
    }

    //ação #35: reconhecimento do fim do comando de repetição
    public void action35() {
        if (!execute) return;
        //desempilhar da pilha de desvios o endereço da instrução de desvio empilhado na ação #34
        Integer endereçoInstruçãoDesvioAcao34 = pilhaDeDesvios.pop();

        //atualizar a instrução de desvio com: endereço ¬ ponteiro + 1
        areaDeInstrucoes
                .stream()
                .filter(i -> i.getPointer() == endereçoInstruçãoDesvioAcao34)
                .findFirst()
                .ifPresent(i -> i.setParameter(new Item(ponteiro + 1)));

        //desempilhar da pilha de desvios o endereço da instrução empilhado na ação #33
        Integer endereçoInstruçãoDesvioAcao33 = pilhaDeDesvios.pop();

        //gerar instrução: (ponteiro, JMP, “endereço”), onde "endereço" é igual ao valor desempilhado
        areaDeInstrucoes.add(new Instruction(ponteiro, "JMP", new Item(endereçoInstruçãoDesvioAcao33)));
        ponteiro++;
    }

    //ação #36: reconhecimento de operação relacional igual
    public void action36() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "EQL", new Item(0)));
        ponteiro++;
    }

    //ação #37: reconhecimento de operação relacional igual
    public void action37() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "DIF", new Item(0)));
        ponteiro++;
    }

    //ação #38: reconhecimento de operação relacional menor que
    public void action38() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "SMR", new Item(0)));
        ponteiro++;
    }

    //ação #39: reconhecimento de operação relacional maior que
    public void action39() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "BGR", new Item(0)));
        ponteiro++;
    }

    //ação #40: reconhecimento de operação relacional menor ou igual
    public void action40() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "SME", new Item(0)));
        ponteiro++;
    }

    //ação #41: reconhecimento de operação relacional maior ou igual
    public void action41() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "BGE", new Item(0)));
        ponteiro++;
    }

    //ação #42: reconhecimento de operação aritmética adição
    public void action42() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "ADD", new Item(0)));
        ponteiro++;
    }

    //ação #43: reconhecimento de operação aritmética subtração
    public void action43() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "SUB", new Item(0)));
        ponteiro++;
    }

    //reconhecimento de operação lógica OU ( | )
    public void action44() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "OR", new Item(0)));
        ponteiro++;
    }

    //ação #45: reconhecimento de operação aritmética multiplicação
    public void action45() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "MUL", new Item(0)));
        ponteiro++;
    }

    //ação #46: reconhecimento de operação aritmética divisão
    public void action46() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "DIV", new Item(0)));
        ponteiro++;
    }

    //ação #47: reconhecimento de operação aritmética divisão inteira
    public void action47() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "DIV", new Item(0)));
        ponteiro++;
    }

    //todo: maquina virtual ainda nao preparada para esta instrução
    //ação #48: reconhecimento de operação aritmética resto da divisão inteira
    public void action48() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "MOD", new Item(0)));
        ponteiro++;
    }

    //ação #49: reconhecimento de operação lógica E (&)
    public void action49() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "AND", new Item(0)));
        ponteiro++;
    }

    //todo: maquina virtual ainda nao preparada para esta instrução
    //ação #50: reconhecimento de operação aritmética potência
    public void action50() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "POW", new Item(0)));
        ponteiro++;
    }

    //ação #51: reconhecimento de identificador de constante ou de variável e tamanho de variável indexada em expressão
    public void action51() {
        if (!execute) return;

        Integer atr1 = tabelaDeSimbolos.get(idArmazenadoAcao24).atr1;
        Integer atr2 = tabelaDeSimbolos.get(idArmazenadoAcao24).atr2;

        if (!variavelIndexada) {
            if (atr2 == null) {
                areaDeInstrucoes.add(new Instruction(ponteiro, "LDV", new Item(atr1)));
                ponteiro++;
            } else {
                erros.add(ERRO3 + idArmazenadoAcao24);
            }

        } else {
            if (atr2 != null) {
                areaDeInstrucoes.add(
                        new Instruction(ponteiro, "LDV", new Item(atr1 + constIntArmazenadaAcao12 - 1)));
                ponteiro++;
            } else {
                erros.add(ERRO8 + idArmazenadoAcao24);
            }
        }
    }

    //ação #52: reconhecimento de constante lógica true
    public void action52() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "LDB", new Item(true)));
        ponteiro++;
    }

    //ação #53: reconhecimento de constante lógica untrue
    public void action53() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "LDB", new Item(false)));
        ponteiro++;
    }

    //ação #54: reconhecimento de operação lógica NÃO ( ! )
    public void action54() {
        if (!execute) return;
        areaDeInstrucoes.add(new Instruction(ponteiro, "NOT", new Item(0)));
        ponteiro++;
    }


    private Boolean identificadorJaDeclarado(String token) {
        if (tabelaDeSimbolos.containsKey(token) ||
                tabelaDeTiposEnumerados.containsKey(token) ||
                tabelaDeTiposEnumerados
                        .entrySet()
                        .stream()
                        .filter(t -> t.getValue().contains(token))
                        .findFirst()
                        .isPresent()) {
            return true;
        }
        return false;
    }


    private Boolean isIdentificadorDeVariavel(String token) {
        return Stream.of(1, 2, 3, 4)
                .filter(n -> n.compareTo(tabelaDeSimbolos.get(token).categoria) == 0)
                .findFirst()
                .isPresent();
    }


    private Boolean isIdentificadorDeConstante(String token) {
        return Stream.of(5, 6, 7)
                .filter(n -> n.compareTo(tabelaDeSimbolos.get(token).categoria) == 0)
                .findFirst()
                .isPresent();
    }


    private Boolean isIdentificadorDeConstanteOuVariavel(String token) {
        return isIdentificadorDeVariavel(token) || isIdentificadorDeConstante(token);
    }


    @Override
    public String toString() {
        return "Tabela {\n" +
                "    contexto = " + contexto + ",\n" +
                "    ponteiro = " + ponteiro + ",\n" +
                "    vt = " + vt + ",\n" +
                "    vp = " + vp + ",\n" +
                "    vi = " + vi + ",\n" +
                "    tvi = " + tvi + ",\n" +
                "    variavelIndexada = " + variavelIndexada + ",\n" +
                "    pilhaDeDesvios = " + pilhaDeDesvios + ",\n" +
                "    areaDeInstrucoes = " + getAreaInstrucoesAsString() + ",\n" +
                "    tabelaDeSimbolos = " + getContentTabelasAsString(tabelaDeSimbolos) + ",\n" +
                "    tupla=" + getContentTabelasAsString(tupla) + ",\n" +
                "    erros=" + getListaDeErrosAsString() + "\n" +
                '}';
    }


    private String getContentTabelasAsString(Map<String, union> tabela) {
        if (tabela.isEmpty()) return "{}";
        List<String> list = tabela.entrySet().stream()
                .map(t -> t.getKey() + " = " + t.getValue().toString())
                .collect(Collectors.toList());
        String r = "{\n";
        for (String s : list) {
            r += "        " + s + "\n";
        }
        return r + "    }";
    }


    private String getAreaInstrucoesAsString() {
        if (areaDeInstrucoes.isEmpty()) return "{}";
        String r = "{\n";
        for (Instruction ai : areaDeInstrucoes) {
            r += "        " + ai.toString() + "\n";
        }
        return r + "    }";
    }


    private String getListaDeErrosAsString() {
        if (erros.isEmpty()) return "{}";
        String r = "{\n";
        for (String e : erros) {
            r += "        " + e + "\n";
        }
        return r + "    }";
    }

    public List<Instruction> getAreaDeInstrucoes() {
        return areaDeInstrucoes;
    }
}
