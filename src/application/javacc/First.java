/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.javacc;
/**
 *
 * @author biankatpas
 */
public class First
{


    static public final RecoverySet header = new RecoverySet();
    static public final RecoverySet declaration = new RecoverySet();
    static public final RecoverySet body = new RecoverySet();
    static public final RecoverySet comentario = new RecoverySet();

    static
    {
        header.add(new Integer(langX.DO));
        declaration.add(new Integer(langX.DECLARATION));
        body.add(new Integer(langX.BODY));
        comentario.add(new Integer(langX.DESCRIPTION));
        comentario.add(new Integer(langX.EOF));
    }
}
