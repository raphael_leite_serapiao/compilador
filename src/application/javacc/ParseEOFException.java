package application.javacc;

public class ParseEOFException extends ParseException
{
    public ParseEOFException (final String sMessage)
    {
        super (sMessage);
    }
}
